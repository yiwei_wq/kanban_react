import uuid from 'node-uuid'
import React from 'react';
import Lanes from './Lanes.jsx';
import LaneActions from '../actions/LaneActions';
import LaneStore from '../stores/LaneStore';
import AltContainer from 'alt/AltContainer';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';

class App extends React.Component {
    render() {
        return (
            <div>
                <button className="add-lane" onClick={this.addItem}>+</button>
                <AltContainer
                    stores={[LaneStore]}
                    inject={ {
                        items: (props) => LaneStore.getState().lanes || []
                    } }
                >
                    <Lanes />
                </AltContainer>
            </div>
        );
    }

    addItem() {
        LaneActions.create({name: 'New lane'});
    }
}

export default DragDropContext(HTML5Backend)(App);
