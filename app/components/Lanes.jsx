import React from 'react';
import Lane from './Lane.jsx';

export default class Lanes extends React.Component {
    render() {
        const lanes = this.props.items;

        return (
            <div className="section">
                <h1>Lanes</h1>
                <div className="lanes">{lanes.map(this.renderLane)}</div>
            </div>
        );
    }

    renderLane(lane) {
        return <Lane className="lane" key={`lane${lane.id}`} lane={lane} />;
    }
}